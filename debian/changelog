gweled (1.0~beta1-1) unstable; urgency=medium

  * New upstream beta release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 31 Mar 2024 19:12:20 +0200

gweled (1.0~alpha-2) unstable; urgency=medium

  * Team Upload
  * remove leftover /var/games ref.
  * drop dpkg-statoverride call (Closes: #1060215)

 -- Alexandre Detiste <tchet@debian.org>  Sun, 07 Jan 2024 19:29:54 +0100

gweled (1.0~alpha-1) unstable; urgency=medium

  * New upstream alpha release:
    - Meson is used for builds;
    - shared high scores are no longer supported;
    - the game uses GTK 3 (closes: #967512);
    - most patches have been merged or are obsolete.
  * Standards-Version 4.6.2, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 02 Dec 2023 18:19:08 +0100

gweled (0.9.1-8) unstable; urgency=medium

  * Apply upstream patch to fix SVGs and allow the game to work with
    current releases of librsvg. Closes: #972084.
  * Switch to debhelper compatibility level 13.
  * Upgrade debian/watch to version 4.
  * Standards-Version 4.6.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 29 Oct 2021 17:57:28 +0200

gweled (0.9.1-7) unstable; urgency=medium

  * Switch back to plain file names now that librsvg is fixed.
    Closes: #923357.

 -- Stephen Kitt <skitt@debian.org>  Wed, 27 Feb 2019 15:25:24 +0100

gweled (0.9.1-6) unstable; urgency=medium

  * Migrate to Salsa.
  * Standards-Version 4.1.4, no further change required.
  * Rewrite debian/copyright in machine-readable format.
  * Delete obsolete 002-release-sound-if-not-used.patch.

 -- Stephen Kitt <skitt@debian.org>  Wed, 09 May 2018 09:03:47 +0200

gweled (0.9.1-5) unstable; urgency=medium

  * Cope with librsvg’s new behaviour by always using URIs instead of
    paths. (Closes: #889688.)
  * Add a missing function declaration.
  * Document old patches.
  * Switch to debhelper compatibility 11, and adjust patches so the
    package can build multiple times in a row.
  * Ship /usr/games/gweled with the desired ownership and permissions,
    instead of using dpkg-statoverride.
  * Standards-Version 4.1.3, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 09 Feb 2018 09:43:43 +0100

gweled (0.9.1-4) unstable; urgency=medium

  * Adopt the package for the Debian Games Team, thanks Ondřej Surý for
    taking care of it for so long! (Closes: #824504.)
  * Build-depend on libmikmod-dev and use pkg-config to find libmikmod.
    (Closes: #745072.)
  * Add upstream fix for timed and endless modes. (Closes: #738455.)
  * Add the manpage written by Braulio Valdivielso Martínez. (Closes:
    #608439.)
  * Clean up modified files to allow building twice in a row.
  * Switch to dpkg-buildflags hardening instead of hardening-wrapper.
  * Fix a couple of spelling mistakes flagged by Lintian.
  * Correct the synopsis so it doesn't start with an article.
  * Clean up debian/control with cme:
    - drop the obsolete version dependency on libgtk2.0-dev;
    - Standards-Version 3.9.8;
    - use canonical URLs for the VCS entries.
  * Drop obsolete build-dependencies (libesd0-dev and quilt).
  * Move the repository to pkg-games.

 -- Stephen Kitt <skitt@debian.org>  Thu, 26 May 2016 18:58:51 +0200

gweled (0.9.1-3) unstable; urgency=low

  * Update watch file
  * Use dh-autoreconf (Closes: #724917)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 02 Oct 2013 13:23:14 +0200

gweled (0.9.1-2) unstable; urgency=low

  * Change homepage and add libesd0-dev build dependency

 -- Ondřej Surý <ondrej@debian.org>  Fri, 01 Apr 2011 16:04:27 +0200

gweled (0.9.1-1) unstable; urgency=low

  * Imported Upstream version 0.9.1 (Closes: #610915)

 -- Ondřej Surý <ondrej@debian.org>  Fri, 01 Apr 2011 13:49:09 +0200

gweled (0.9-1) unstable; urgency=low

  * Imported Upstream version 0.9 (Closes: #610915)
  * Debian packaging:
    + Add a watch file

 -- Ondřej Surý <ondrej@debian.org>  Mon, 24 Jan 2011 14:00:10 +0100

gweled (0.8.repack-5) unstable; urgency=medium

  * Add autoreconf call after patching src/Makefile.am to use sound.{h,c}
  * Make intltool-update ignore files in quilt .pc/ directory
  * Only remove the content of /var/games, not the directory itself
    (Closes: #595590)
  * Bump standards version to 3.9.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 13 Sep 2010 14:31:54 +0200

gweled (0.8.repack-4) unstable; urgency=low

  * Add /var/games to debian/dirs (Closes: #591597)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 04 Aug 2010 09:28:45 +0200

gweled (0.8.repack-3) unstable; urgency=low

  * Fix bashism and typo in debian/postrm

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Jul 2010 19:03:10 +0200

gweled (0.8.repack-2) unstable; urgency=low

  * Capitalize GNOME to shut-up lintian (Closes: #585797)
  * Remove autotools dependency, leave only intltool
  * Add handling of score files (Closes: #587043)
  * Remove /var/games before packing the package

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Jul 2010 18:57:02 +0200

gweled (0.8.repack-1) unstable; urgency=low

  * Imported Upstream version 0.8.repack (now for real) (Closes: #582829)
  * Fix extra line in debian/rules
  * Change debhelper compatibility level to 7
  * Update build dependencies to include only GTK+, rsvg2 and mikmod.

 -- Ondřej Surý <ondrej@debian.org>  Mon, 24 May 2010 16:26:14 +0200

gweled (0.8-1) unstable; urgency=low

  * New upstream version 0.8 (Closes: #582028)
  * New upstream author: Daniele Napolitano
  * Regenerate control. (Closes: #554070, #523541)
  * All patches merged upstream, all Debian bugs fixed.
    Closes: #398250, #406884, #402169

 -- Ondřej Surý <ondrej@debian.org>  Tue, 18 May 2010 11:42:59 +0200

gweled (0.7-2) unstable; urgency=low

  * Disable disk writer driver in MikMod initialization
    (Closes: #401727, #401375)
  * Fix double free when exiting (Closes: #401730)

 -- Ondřej Surý <ondrej@debian.org>  Tue,  5 Dec 2006 18:20:44 +0100

gweled (0.7-1) unstable; urgency=low

  * New upstream release. (Closes: #392973)
    + Removed some patches merged upstream.
  * Added -rdynamic CFLAG (Closes: #392971)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Oct 2006 14:14:41 +0200

gweled (0.6-2) unstable; urgency=low

  * Fix missing DESTDIR in scoredir (Closes: #307643)

 -- Ondřej Surý <ondrej@sury.org>  Fri,  6 May 2005 21:25:38 +0200

gweled (0.6-1) unstable; urgency=low

  * New upstream release.
  * Apply patch to fix timed game speed not resetting (Closes: #286874)
    - credits to Neil Moore <neil@s-z.org>
  * Apply patch to fix FTBFS on amd64 (Closes: #286956)
    - credits to Andreas Jochens <aj@andaco.de>

 -- Ondřej Surý <ondrej@sury.org>  Tue,  3 May 2005 20:48:13 +0200

gweled (0.5-2) unstable; urgency=low

  * Fix PPC runtime error, credits to Michael Klein (Closes: #280506)

 -- Ondřej Surý <ondrej@sury.org>  Wed, 10 Nov 2004 12:37:17 +0100

gweled (0.5-1) unstable; urgency=low

  * New upstream release.
  * Remove upstream score file from package.
  * Move binary to /usr/games and make it setgid games (Closes: #268682)
  * Create debian menu entry (Closes: #268682)

 -- Ondřej Surý <ondrej@debian.org>  Thu,  2 Sep 2004 09:34:12 +0200

gweled (0.4-1) unstable; urgency=low

  * Initial Release. (Closes: #225711)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Aug 2004 08:58:17 +0200
